# MOS on Google Cloud

## Installation Document
- [ ] https://www.mathworks.com/content/dam/mathworks/mathworks-dot-com/products/matlab-online-server/matlab-online-server-install-and-admin-guide.pdf

## MOS Location
- [ ] (MOS with nginx load balancer) https://yingmosproject.matlabonlineserver.com 
- [ ] (MOS with GCP load balancer) http://yingmosingress.matlabonlineserver.com (Possible Authnz bug)

<details>
<summary><h2>Network Topology</h2></summary>

- [ ] Deployed system network topology \
![alt text](image/network.PNG "Network Topology")

</details>
<details>
<summary><h2>Google Environment</h2></summary>

### Project
- [ ] All resources should be under the same project. I have all my resources allocated under project **epsmos**

<details>
  <summary><h3>Registry</h3></summary>

- [ ] Google now suggests using Artifact Registry instead of Container Registry
- [ ] After creating repository, the url can be fetched from GCP Console (highlighted below) \
![alt text](image/artifactregistry.PNG "Artifact Registry")
- [ ] The MOS install.config file should be configured accordingly based on Artifact Registry
```
DOCKER_REGISTRY=us-east1-docker.pkg.dev/epsmos
DOCKER_REPOSITORY=yingmos
```

</details>
<details>
  <summary><h3>VPC</h3></summary>

- [ ] Instead of using the default VPC, a new VPC called **vpc-ying** is created. All resources are under this VPC
- [ ] Firewall rules are created under this VPC to allow external access to HTTP/HTTPS, ssh, license, nfs and all access from MathWorks \
![alt text](image/firewallrules.PNG "Firewall Rules")

</details>
<details>
  <summary><h3>VM</h3></summary>

- [ ] license server
  - Used to host the license for MATLAB Online Server and MATLAB. e2-medium with 20G
- [ ] staging server
  - A linux machine used as the workstation to host MOS/MATLAB, kubernetes command and google commands.
  - e2-medium with 60G disk space

</details>
<details>
<summary><h3>Cluster</h3></summary>

- [ ] Cluster is created by using the UBUNTU image, and also enabling the auto-scaling option, example commands below
```
gcloud config set project epsmos
gcloud config set compute/zone us-east1-b
gcloud config set compute/region us-east1

gcloud container clusters create ying-mos-cluster --no-enable-basic-auth --release-channel regular --machine-type n2-standard-2 --image-type UBUNTU_CONTAINERD --disk-type "pd-standard" --disk-size 200 --metadata disable-legacy-endpoints=true --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" --max-pods-per-node 110 --num-nodes 2 --logging=SYSTEM,WORKLOAD --monitoring=SYSTEM --enable-ip-alias --network "projects/epsmos/global/networks/vpc-ying" --subnetwork "projects/epsmos/regions/us-east1/subnetworks/vpc-ying-us-east" --no-enable-intra-node-visibility --default-max-pods-per-node 110 --no-enable-master-authorized-networks --addons HorizontalPodAutoscaling,HttpLoadBalancing,GcePersistentDiskCsiDriver --enable-autoupgrade --enable-autorepair --max-surge-upgrade 1 --max-unavailable-upgrade 0 --enable-shielded-nodes --enable-autoscaling --min-nodes 1 --max-nodes 10

kubectl create clusterrolebinding cluster-admin-binding --clusterrole cluster-admin --user yliu@mathworks.com

gcloud container clusters get-credentials ying-mos-cluster --region us-east1-b
```

</details>
<details>
  <summary><h3>Storage</h3></summary>

- [ ] GCP Filestore is used, created directly using the GCP console \
![alt text](image/filestore.PNG "GCP Filestore")
- [ ] Create PV (pv.yaml): nfs server and path value is taken from Filestore. 
```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: yingpv
spec:
  capacity:
    storage: 1T 
  accessModes:
  - ReadWriteMany
  storageClassName: "premium-rwx"
  nfs:
    path: /yingmosfileshare
    server: 10.79.201.2 
```
- [ ] Create PVC (pvc.yaml): 
```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: yingpvc
  namespace: mathworks
spec:
  # Specify "" as the storageClassName so it matches the PersistentVolume's StorageClass.
  # A nil storageClassName value uses the default StorageClass. For details, see
  # https://kubernetes.io/docs/concepts/storage/persistent-volumes/#class-1
  accessModes:
  - ReadWriteMany
  storageClassName: "premium-rwx"
  volumeName: yingpv
  resources:
    requests:
      storage: 1T 
```
- [ ] Testing Pod: can see that pvc is successfully mounted to the pod is all configuration is right.
```
apiVersion: v1
kind: Pod
metadata:
  name: fileshare-test-pod
  namespace: mathworks
spec:
  containers:
  - name: test-container
    image: nginx
    volumeMounts:
    - mountPath: /mnt/pvc
      name: yingpvc
  volumes:
  - name: yingpvc
    persistentVolumeClaim:
      claimName: yingpvc
      readOnly: false
```

</details>
<details>
<summary><h3>HTTPS Load Balancer</h3></summary>

- [ ] The HTTPS load balancer is actually configured AFTER MOS is installed and configured.
- [ ] Create Self Managed Certificate
```
openssl req -new -newkey rsa:2048 -nodes -keyout ying-ingress-tls.key -out ying-ingress-tls.csr -subj "/C=US/CN=matlabonlineserver.com"
openssl x509 -req -days 365 -in ying-ingress-tls.csr -signkey ying-ingress-tls.key -out ying-ingress-tls.crt

kubectl delete secret ying-tls-secret -n mathworks
kubectl create secret tls ying-tls-secret --cert ying-ingress-tls.crt --key ying-ingress-tls.key -n mathworks
```

</details>
</details>

<details>
<summary><h2>License Server</h2></summary>

- [ ] Installed both MOS license and MATLAB network license
- [ ] Fixed vendor port for firewall settings. This highlighted ports are configured in the VPC firewall rules (**license2700** and **mlmport**)\
![alt text](image/license.PNG "License Example")

</details>

<details>
<summary><h2>MOS features</h2></summary>
<details>
<summary><h3>OKTA</h3></summary>

- [ ] Detailed configuration doc is [here](Note/OKTA.docx)
- [ ] OKTA does not work with GCP Ingress load balancer, need to debug
- [ ] Used subject.displayName instead of subject.subjectId to have better formatted directory name. (subject.displayName can be free text. subject.sujectId is email address.)
- [ ] Need to map uid and displayName in OKTA SAML configuration \
![alt text](image/okta.PNG "License Example")
- [ ] Sample authnz.yaml
```
identityProviders:
  - id: saml
    type: saml
    relyingPartyId: https://yingmosproject.matlabonlineserver.com/service/assertionConsumer 
    assertionConsumerPath: /service/assertionConsumer
    subjectAttributeMapping:
      displayName: "displayName"
      groups: "group"
      extra:
        email: "email"
        uid: "uidNumber"
        gid: "gidNumber"
    corsAllowOriginDomain: https://dev-08170537.okta.com
    idpMetadataUrl: https://dev-08170537.okta.com/app/exk1cvmrjdbsYIQuP5d7/sso/saml/metadata
```

</details>
<details>
<summary><h3>Multiple MATLAB Version Support</h3></summary>

- [ ] Tagging: MOS 2022a will tag the MATLAB image with R2022a. If MATLAB versiond does not match with MOS version, might need to manually tag the MATLAB docker image with right version.
```
./mosadm build-matlab-image /MATLAB/R2021b/
docker tag mathworks/mos/com.mathworks.matlabonlineserver.matlab-image:R2022a us-east1-docker.pkg.dev/epsmos/yingmos/com.mathworks.matlabonlineserver.matlab-image:R2021b
```
- [ ] Manually push MATLAB version ( not R2022a in this case ) to Artifact Repository
```
docker push us-east1-docker.pkg.dev/epsmos/yingmos/com.mathworks.matlabonlineserver.matlab-image:R2021b
```
- [ ] matlab pool yaml file need to add image tagging info in order to fetch/map the right MATLAB image
```
replicaCount: 1
hostPathMatlab:
  enabled: false
  path: "/opt/matlab/R2021b"
poolConfig:
  configId: "R2021b"
  displayName: "R2021b"
flexlm:
  servers: "27000@34.148.132.118"
storage:
  bindfs:
    enabled: true
  profiles:
    - name: nfsmount
      startDirectory: "/code"
      addons:
        directory: "/MATLAB/MATLAB Add-Ons"
      mounts:
        - name: empty
          mountPath: /code
          type: empty
          permissionType: none
security:
  apparmor:
    enabled: false
  selinux:
    enabled: false
images:
  matlabEmbedded:
    tag: "R2021b"
```

</details>
<details>
<summary><h3>Cloud Storage</h3></summary>

- [ ] NFS mount
  - Note: subFolder value maps to the NFS server side path, defines which folder in NFS should be mounted to the pod, it is an absolution value
    - NFS server mount end point
  ```
  34.148.132.118:/mnt/yingdisk
  ```
    - MOS side config: subPath value has to be an existing directory name on the NFS server side.
  ```
  - name: nfsmount
          permissionType: user
          uid: "${subject.uid}"
          gid: "${subject.uid}"
          mountPath: "/mnt/nfs/${subject.displayName}"
          type: nfs
          server: "34.148.132.118"
          subPath: "/mnt/yingdisk/data/${subject.displayName}"
          mountOptions: "rw,relatime,vers=3,rsize=1048576,wsize=1048576,namlen=255,acregmin=600,acregmax=600,acdirmin=600,acdirmax=600,hard,nocto,noacl,proto=tcp,timeo=600,retrans=2,mountproto=tcp,local_lock=none,nolock"
  ```
- [ ] PVC mount
  - Note: subFolder value maps to PVC's sub directory
  - Sample yaml file configuration: subPath, /data, already exists in the pvc. Can verify from the testing pod.
```
 - name: pvc
          mountPath: "/pvc/${subject.displayName}"
          type: pvc
          claimName: yingpvc
          subPath: "/data"
          permissionType: user
          uid: "${subject.uid}"
          gid: "${subject.uid}"
```

</details>
<details>
<summary><h3>Load Balancer</h3></summary>

- [ ] The ingest service yaml file can be located [here](Note/ingress/ingress.yaml)
- [ ] tls section with right kubenetes secret name enables the https: ying-tls-secret is the secret name \
```

apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ying-ingress
  namespace: mathworks
spec:
  tls:
  - secretName: ying-tls-secret
  rules:
  - host: yingtlstest.matlabonlineserver.com
```
- [ ] MOS interfaces values should be all included in the ingress load balancer backend value
```
- host: yingmosingress.matlabonlineserver.com
    http:
      paths:
      - backend:
          service:
            name: authnz
            port:
              number: 80
        path: /_authnz
        pathType: Prefix
      - backend:
          service:
            name: authnz
            port:
              number: 80
        path: /service/core/authnz
        pathType: Prefix
      - backend:
          service:
            name: authnz
            port:
              number: 80
        path: /authnz
        pathType: Prefix
      - backend:
          service:
            name: core-ui
            port:
              number: 80
        path: /
        pathType: Prefix
```
- [ ] Detailed MOS backend info can be fetched from ingress console, mapped with service names and frontends\
![alt text](image/ingress.PNG "Ingress Configuration")

</details>

</details>
<details>
  <summary><h2>Add-Ons and Example BigQuery Code</h2></summary>

<details>

