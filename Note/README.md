# MOS on Google Cloud



## MOS Location
- [ ] (MOS with nginx load balancer) https://yingmosproject.matlabonlineserver.com 
- [ ] (MOS with GCP load balancer) http://yingmosingress.matlabonlineserver.com

# Installation Document
- [ ] https://www.mathworks.com/content/dam/mathworks/mathworks-dot-com/products/matlab-online-server/matlab-online-server-install-and-admin-guide.pdf

## Network Topology
![alt text](image/test.PNG "Network Topology")
## Google Environment
### Registry
- [ ] Google now suggests using Artifact Registry instead of Container Registry
- [ ] After creating repository, the url can be fetched from GCP Console (highlighted below)
![alt text](image/artifactregistry.PNG "Artifact Registry")
- [ ] The MOS install.config file should be configured accordingly based on Artifact Registry
```
DOCKER_REGISTRY=us-east1-docker.pkg.dev/epsmos
DOCKER_REPOSITORY=yingmos
```
### Project
- [ ] All resources should be under the same project. I have all my resources allocated under project **epsmos**
### VPC
- [ ] Instead of using the default VPC, I created a new VPC called **vpc-ying**. All resources are under this VPC
- [ ] Firewall rules are created under this VPC to allow external access to HTTP/HTTPS, ssh, license, nfs and all access from MathWorks
![alt text](image/firewallrules.PNG "Firewall Rules")

### VM
- [ ] license server
- Used to host the license for MATLAB Online Server and MATLAB. e2-medium with 20G
- [ ] staging server
- A linux machine used as the workstation to host MOS/MATLAB, kubernetes command and google commands.
- e2-medium with 60G disk space
### Cluster
- [ ] Cluster is created by using the UBUNTU image, and also enabling the auto-scaling option, example commands below
```
gcloud config set project epsmos
gcloud config set compute/zone us-east1-b
gcloud config set compute/region us-east1

gcloud container clusters create ying-mos-cluster --no-enable-basic-auth --release-channel regular --machine-type n2-standard-2 --image-type UBUNTU_CONTAINERD --disk-type "pd-standard" --disk-size 200 --metadata disable-legacy-endpoints=true --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" --max-pods-per-node 110 --num-nodes 2 --logging=SYSTEM,WORKLOAD --monitoring=SYSTEM --enable-ip-alias --network "projects/epsmos/global/networks/vpc-ying" --subnetwork "projects/epsmos/regions/us-east1/subnetworks/vpc-ying-us-east" --no-enable-intra-node-visibility --default-max-pods-per-node 110 --no-enable-master-authorized-networks --addons HorizontalPodAutoscaling,HttpLoadBalancing,GcePersistentDiskCsiDriver --enable-autoupgrade --enable-autorepair --max-surge-upgrade 1 --max-unavailable-upgrade 0 --enable-shielded-nodes --enable-autoscaling --min-nodes 1 --max-nodes 10

kubectl create clusterrolebinding cluster-admin-binding --clusterrole cluster-admin --user yliu@mathworks.com

gcloud container clusters get-credentials ying-mos-cluster --region us-east1-b
```
### Storage
- [ ] GCP Filestore is used, created directly using the GCP console
![alt text](image/filestore.PNG "GCP Filestore")
## License Manager
### MOS license
### MATLAB license
### Port configuration
## MOS features
### HTTPS
### Add-Ons
### OKTA
### Multiple MATLAB Version Support
### Cloud Storage
### Load Balancing
### Example BigQuery code

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/YingMATLAB/mos-on-google-cloud.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/YingMATLAB/mos-on-google-cloud/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
